#ifndef FUNCTIONS_C
#define FUNCTIONS_C

typedef struct odjeca {
	int id;
	char marka[20];
	char cijena[5];
	char velicina[3];
	char kolicina[5];
}ODJECA;

void kreiranjeDatoteke(const char*, unsigned int*);
void izbornik(char*, unsigned int*);
void unosOdjece(char*, unsigned int*);
void pretrazivanjeNaziv(char*, unsigned int*);
void brisanje(char*, unsigned int*);
void sortiranje(char*, unsigned int*);
void izmjenaPodataka(char*, unsigned int*);
void lista(char*, unsigned int*);
void brisanjeDatoteke(char*, unsigned int*);
void izlazPrograma(void);

#endif //FUNCTIONS_C