#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "header.h"
FILE* fp = NULL;

void kreiranjeDatoteke(const char* imeDatoteke, unsigned int* broj) {

	fp = fopen(imeDatoteke, "rb");
	if (fp == NULL) {
		perror("Datoteka se ne moze otvoriti!");

		fp = fopen(imeDatoteke, "wb");
		if (fp == NULL) {
			perror("Datoteka se ne moze kreirati!");
			exit(EXIT_FAILURE);
		}
		else
		{
			fwrite(broj, sizeof(unsigned int), 1, fp);
			fclose(fp);
			printf("Datoteka je uspjeno kreirana");
		}
	}
	else {
		fread(broj, sizeof(unsigned int), 1, fp);
		fclose(fp);
	}
}

void izbornik(char* imeDatoteke, unsigned int* broj) {

	int uvijet;

	do {

		printf("\n==============SKLADISTE ODJECE==============\n");
		printf("1. Unos odjece.\n");
		printf("2. Trazenje odjece preko naziva.\n");
		printf("3. Brisanje odjece prema (kolicini) stanju.\n");
		printf("4. Izmjena unesenih podataka.\n");
		printf("5. Sortiranje po cijeni odjece.\n");
		printf("6. Pregled liste odjece.\n");
		printf("7. Brisanje datoteke.\n");
		printf("8. Izlaz programa.\n");
		printf("============================================\n");
		do {
			scanf("%d", &uvijet);
		} while (uvijet > 8 || uvijet < 1);

		switch (uvijet) {
		case 1:
			system("cls");
			unosOdjece(imeDatoteke, broj);
			break;
		case 2:
			system("cls");
			pretrazivanjeNaziv(imeDatoteke, broj);
			break;
		case 3:
			system("cls");
			brisanje(imeDatoteke, broj);
			break;
		case 4:
			system("cls");
			izmjenaPodataka(imeDatoteke, broj);
			break;
		case 5:
			system("cls");
			sortiranje(imeDatoteke, broj);
			break;
		case 6:
			system("cls");
			lista(imeDatoteke, broj);
			break;
		case 7:
			system("cls");
			brisanjeDatoteke(imeDatoteke, broj);
			break;
		case 8:
			system("cls");
			izlazPrograma();
			break;
		default:
			system("cls");
			printf("\nUnos nije dobar.\n");
		}
	} while (1);
}

void unosOdjece(char* imeDatoteke, unsigned int* broj) {

	fp = fopen(imeDatoteke, "rb+");
	if (fp == NULL) {
		perror("Dodavanje odjece u datoteke odjeca.bin nije moguce!");
		return;
	}
	else {
		ODJECA temp = { 0 };
		
		getchar();
		printf("Unesite naziv odjece:\n");
		scanf("%19s", temp.marka);
		printf("Unesite cijenu odjece:\n");
		getchar();
		scanf("%4s", temp.cijena);
		printf("Unesite broj/velicinu odjece(npr. 43) :\n");
		getchar();
		scanf("%2s", temp.velicina);
		printf("Unesite kolicinu odjece(npr. jedan par patika kolicina 1):\n");
		getchar();
		scanf("%4s", temp.kolicina);
		getchar();
		fseek(fp, sizeof(unsigned int) + ((*broj) * sizeof(ODJECA)), SEEK_SET);//pomicanje u datoteci
		temp.id = 1+(*broj)++;
		fwrite(&temp, sizeof(ODJECA), 1, fp);//sprema strukturu 
		rewind(fp);
		fwrite(broj, sizeof(unsigned int), 1, fp);//elementi koji ce se zapisati u binarnu datoteku
		fclose(fp);
	}
}
void pretrazivanjeNaziv(char* imeDatoteke, unsigned int* broj) {

	fp = fopen(imeDatoteke, "rb");
	char naziv[20] = { '\0' };
	unsigned int i, br = 0;
	if (imeDatoteke == NULL) {
		printf("Nije moguce otvoriti datoteku odjeca.bin!\n");
		return;
	}
	else {

		ODJECA* odjeca = NULL;
		fread(broj, sizeof(unsigned int), 1, fp);

		if (*broj == 0) {
			printf("Nije unesena niti jedna roba.\n");
			fclose(fp);
			return;
		}
		else {

			odjeca = (ODJECA*)calloc(*broj, sizeof(ODJECA));
			if (odjeca == NULL) {
				printf("Neuspjesno zauzimanje memorije\n");
				return;
			}
			else {
				
				fread(odjeca, sizeof(ODJECA), *broj, fp);
				fclose(fp);
				printf("Unesite naziv odjece.\n");
				scanf("%19s", &naziv);
				system("cls");
				printf(" ID\tMARKA\t\tKOLICINA\tVELICINA\tCIJENA\n");
				for (i = 0; i < *broj; i++) {
					if (strcmp((odjeca + i)->marka, naziv) == 0) {
						br++;
						printf(" %d\t%s\t\t%s\t\t%s\t\t%s\n", (odjeca + i)->id, (odjeca + i)->marka, (odjeca + i)->kolicina, (odjeca + i)->velicina, (odjeca + i)->cijena);
					}
				}
				if (br == 0) {
					printf("Naziv odjece koji ste unjeli %s nije unesen u datotektu!\n", naziv);
				}
				free(odjeca);
			}
		}
	}
}
void brisanje(char* imeDatoteke, unsigned int* broj) {

	fp = fopen(imeDatoteke, "rb");
	unsigned int i, br = 0;
	char stanje[5] = { '\0' };

	if (fp == NULL) {
		printf("Nije moguce otvoriti datoteku odjeca.bin!\n");
		return;
	}
	else {
		ODJECA* odjeca = NULL;
		fread(broj, sizeof(int), 1, fp);

		if (*broj == 1) {

			printf("Nije upisana niti jedan naziv odjece.\n");
			fclose(fp);
			return;
		}
		else {

			odjeca = (ODJECA*)calloc(*broj, sizeof(ODJECA));


			if (odjeca == NULL) {
				printf("Zauzimanje memorije nije uspjelo!\n");
				return;
			}
			else {
				system("cls");
				fread(odjeca, sizeof(ODJECA), *broj, fp);// pokazivac na memorijski prostor u koji ce se zapisati sadr�aj iz binarne datoteke
				fclose(fp);
				printf("Unesite stanje koje zelite obrisati:\n");
				scanf("%4s", &stanje);
				for (i = 0; i < *broj; i++) {
					if (strcmp((odjeca + i)->kolicina, stanje) == 0) {
						br++;
						(*broj)--;
						break;
					}
				}
				if (br == 0) {
					printf("Kolicina koja ste unjeli %s nije unesan u datoteku!\n", stanje);
					free(odjeca);
					return;
				}
				fp = fopen(imeDatoteke, "wb");
				if (fp == NULL) {
					printf("Nije moguce otvoriti datoteku odjeca.bin\n");
					free(odjeca);
					return;
				}
				else {
					fwrite(broj, sizeof(int), 1, fp);
					br = 0;
					for (i = 0; i < (*broj) + 1; i++) {
						if (strcmp((odjeca + i)->kolicina, stanje) == 0) {
							br++;
							continue;
						}
						else {
							if (br == 0) {
								fwrite((odjeca + i), sizeof(ODJECA), 1, fp);
							}
							else {
								(odjeca + i)->id = (odjeca + i)->id--;
								fwrite((odjeca + i), sizeof(ODJECA), 1, fp);
							}
						}
					}
					fclose(fp);
					free(odjeca);
				}
			}
		}
	}
}

void sortiranje(char* imeDatoteke, unsigned int* broj) {

	ODJECA* odjeca = NULL;
	ODJECA* odjeca2 = NULL;
	fp = fopen(imeDatoteke, "rb");
	unsigned int i, j;
	if (fp == NULL) {
		printf("Nije moguce otvoriti datoteku odjeca.bin!\n");
	}
	else {

		fread(broj, sizeof(int), 1, fp);
		if (*broj == 0) {
			printf("Nije upisana niti jedan naziv odjece.\n");
			fclose(fp);
			return;
		}
		else {
			ODJECA* odjeca = NULL;
			odjeca = (ODJECA*)calloc(*broj, sizeof(ODJECA));

			if (odjeca == NULL) {
				printf("Zauzimanje memorije nije uspjelo!\n");
				return;
			}
			odjeca2 = (ODJECA*)calloc(*broj, sizeof(ODJECA));
			if (odjeca2 == NULL) {
				printf("Zauzimanje memorije nije uspjelo!\n");
				return;
			}
			fread(odjeca, sizeof(ODJECA), *broj, fp);
			fclose(fp);
			for (i = 0; i <= *broj - 1; i++) {
				for (j = i + 1; j <= (*broj) - 1; j++) {
					int prvi = atoi((odjeca + i)->cijena);
					int drugi = atoi((odjeca + j)->cijena);
					if (prvi > drugi) {
						memcpy(odjeca2, (odjeca + i), sizeof(ODJECA));
						memcpy((odjeca + i), (odjeca + j), sizeof(ODJECA));
						memcpy((odjeca + j), odjeca2, sizeof(ODJECA));
					}
				}
			}printf(" ID\tMARKA\t\tKOLICINA\tVELICINA\tCIJENA\n");
			for (i = 0; i <= *broj - 1; i++) {
				printf(" %d\t%s\t\t%s\t\t%s\t\t%s\n", (odjeca + i)->id, (odjeca + i)->marka, (odjeca + i)->kolicina, (odjeca + i)->velicina, (odjeca + i)->cijena);
			}
			free(odjeca);
			free(odjeca2);
		}
		return;
	}
}

void izmjenaPodataka(char* imeDatoteke, unsigned int* broj) {

	fp = fopen(imeDatoteke, "rb");
	if (fp == NULL) {
		printf("Nije moguce otvoriti datoteku odjeca.bin!\n");
	}
	else {
		fread(broj, sizeof(unsigned int), 1, fp);

		if (*broj == 0) {
			printf("Nije upisana niti jedan naziv odjece.\n");
			fclose(fp);
			return;
		}
		else {
			ODJECA* odjeca = NULL;
			odjeca = (ODJECA*)calloc(*broj, sizeof(ODJECA));

			if (odjeca == NULL) {
				printf("Zauzimanje memorije nije uspjelo!\n");
				return;
			}
			ODJECA privremenaroba = { 0 };
			fread(odjeca, sizeof(ODJECA), *broj, fp);
			unsigned int i;
			printf("Unesite ID robe koje zelite pormjeniti: ");
			unsigned int trenutniID = 0;
			scanf("%d", &trenutniID);
			unsigned int pronalazak = 0;
			unsigned int indesk = -1;
			for (i = 0; i < *broj; i++) {
				if (trenutniID == (odjeca + i)->id) {
					pronalazak = 1;
					indesk = i;
				}
			}
			if (pronalazak) {
				system("cls");
				printf("Pronadena roba je:\n");
				printf(" ID\tMARKA\t\tKOLICINA\tVELICINA\tCIJENA\n");
				printf(" %d\t%s\t\t%s\t\t%s\t\t%s\n", (odjeca + indesk)->id, (odjeca + indesk)->marka, (odjeca + indesk)->kolicina, (odjeca + indesk)->velicina, (odjeca + indesk)->cijena);
				fclose(fp);
				fp = fopen(imeDatoteke, "rb+");
				printf("Unosite izmjenjene podatke o robi\n");
				printf("Novi naziv marke: ");
				scanf("%19s", privremenaroba.marka);
				printf("Nova cijena robe: ");
				scanf("%4s", privremenaroba.cijena);
				printf("Nova velicina robe: ");
				scanf("%2s", privremenaroba.velicina);
				printf("Nova kolicina robe: ");
				scanf("%4s", privremenaroba.kolicina);
				privremenaroba.id = (odjeca + indesk)->id;
				fseek(fp, sizeof(unsigned int) + ((indesk) * sizeof(ODJECA)), SEEK_SET);
				fwrite(&privremenaroba, sizeof(ODJECA), 1, fp);
				fclose(fp);
				int nastavak;
				printf("\nUnesite bilo koji broj za nastavak\n");
				scanf("%d", &nastavak);
			}
			else {
				int nastavak;
				printf("Roba ne postoji.\n");
				printf("\nUnesite bilo koji broj za nastavak: ");
				scanf("%d", &nastavak);
			}
			free(odjeca);
		}
	}
}
void lista(char* imeDatoteke, unsigned int* broj) {

	fp = fopen(imeDatoteke, "rb");
	unsigned int i;
	if (imeDatoteke == NULL) {
		printf("Nije moguce otvoriti datoteku odjeca.bin!\n");
	}
	else {
		fread(broj, sizeof(unsigned int), 1, fp);

		if (*broj == 0) {
			printf("Nije upisana niti jedan naziv odjece.\n");
			fclose(fp);
			return;
		}
		else {
			ODJECA* odjeca = NULL;
			odjeca = (ODJECA*)calloc(*broj, sizeof(ODJECA));

			if (odjeca == NULL) {
				printf("Zauzimanje memorije nije uspjelo!\n");
				return;
			}
			else {
				fread(odjeca, sizeof(ODJECA), *broj, fp);
				fclose(fp);
				printf(" ID\tMARKA\t\tKOLICINA\tVELICINA\tCIJENA\n");
				for (i = 0; i < *broj; i++) {
					printf(" %d\t%s\t\t%s\t\t%s\t\t%s\n", (odjeca + i)->id, (odjeca + i)->marka, (odjeca + i)->kolicina, (odjeca + i)->velicina, (odjeca + i)->cijena);
				}
				free(odjeca);
			}
		}
	}
}

void brisanjeDatoteke(char* imeDatoteke, unsigned int* broj) {

	system("cls");
	printf("Jeste li sigurni da zelite obrisati datoteku %s?\n", imeDatoteke);
	printf("Unesite [da] ako uistinu zelite obrisati datoteku u  suprotnom unesite [ne].\n");
	char potvrda[3] = { '\0' };
	scanf("%2s", potvrda);
	if (!strcmp("da", potvrda)) {
		remove(imeDatoteke) == 0 ? printf("Uspjesno obrisana datoteka %s!\n",
			imeDatoteke) : printf("Neuspjesno brisanje datoteke %s!\n", imeDatoteke);
	}
	(*broj) = 0;
	kreiranjeDatoteke(imeDatoteke, broj);

}
void izlazPrograma(void) {

	system("cls");
	printf("Jeste li sigurni da zelite izaci iz programa?][da/ne]\n");
	char potvrda[3] = { '\0' };
	scanf("%2[^\n]", potvrda);
	if (!strcmp("da", potvrda)) {
		exit(EXIT_FAILURE);
	}
}